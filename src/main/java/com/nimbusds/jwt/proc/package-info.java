/**
 * Interfaces and classes which can be used for application-specific processing
 * of JSON Web Tokens (JWTs).
 */
package com.nimbusds.jwt.proc;