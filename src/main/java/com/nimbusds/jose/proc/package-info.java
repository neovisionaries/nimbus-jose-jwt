/**
 * Interfaces and classes which can be used for application-specific processing
 * of JOSE objects.
 */
package com.nimbusds.jose.proc;