package com.nimbusds.jose.proc;


import java.util.HashMap;


/**
 * Simple map-based context.
 *
 * @author Vladimir Dzhuvinov
 * @version $version$ (2015-04-22)
 */
public class SimpleContext extends HashMap<String,Object> implements Context {


}
