package com.nimbusds.jose;


/**
 * JOSE algorithm provider.
 *
 * @author  Vladimir Dzhuvinov
 * @version $version$ (2015-04-20)
 */
public interface AlgorithmProvider { }
