/**
 * Java Cryptography Architecture (JCA) interfaces and classes.
 */
package com.nimbusds.jose.jca;